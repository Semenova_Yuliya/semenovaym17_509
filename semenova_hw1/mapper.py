
# coding: utf-8

# In[ ]:


#!/usr/bin/env python
"""mapper.py"""
import sys 

#Основная функция маппера: выделяет из строки IP и объём, создаёт кортеж с ключём, счётчиком и значением
def gener(doc):
    word = doc[:doc.find(' - - ')] #Находим IP
    request = doc[doc.find('" ') + 2:].split(' ') #рвзбиваем строку на слова, разделённые пробелом
    try:
        vol = int(request[1]) #Если объём запроса - число, записываем его в кортеж
    except:
        vol = 0 #Если нет, записываем 0
    yield word, 1, vol

#Выводим результат разбиения
def mapper(stdin):
	for line in stdin:
		for key, count, value in gener(line):
			print(key +"\t"+ str(count) + "\t" + str(value))
			
if __name__ == '__main__':
	mapper(sys.stdin)

