
# coding: utf-8

# In[9]:


#!/usr/bin/env python
"""combiner.py"""
import sys

#Редьюс
def do_reduce(word, counts, values): 
    return word, sum(counts),sum(values)
 
prev_key = None 
keys = []
dataset = []
values = [] 
counts = []
sets = []

#Сортировка входных данных по ip
for line in sys.stdin:
    key,count,vol = line.split("\t")
    if key in keys:
        dataset[keys.index(key)].append((key,int(count),int(vol)))
    elif key not in keys or len(keys) == 0:
        keys.append(key)
        dataset.append([(key,int(count),int(vol))])   

for arr in dataset:
    sets += arr
arr,dataset = None,None

#собственно, комбинирование
for line in sets: 
    key, count, value = line 
    if key != prev_key and prev_key is not None: 
        result_key, result_count, result_value = do_reduce(prev_key, counts, values) 
        print(result_key + "\t" + str(result_count) + "\t" + str(result_value)) 
        values = [] 
        counts = []
    prev_key = key 
    values.append(int(value))
    counts.append(int(count))

if prev_key is not None: 
    result_key, result_count, result_value = do_reduce(prev_key, counts, values) 
    print(result_key + "\t" + str(result_count) + "\t" + str(result_value))  

