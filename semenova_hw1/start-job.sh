#!/bin/sh
sudo /usr/local/hadoop/bin/hdfs dfs -rm -r /output/out
sudo /usr/local/hadoop/bin/hadoop jar /usr/local/hadoop/share/hadoop/tools/lib/hadoop-streaming-2.7.7.jar -D mapreduce.job.reduces=5 -input /input/log-file.txt -output /output/out -file /mnt/c/tmp/semenova_hw1/mapper.py -file /mnt/c/tmp/semenova_hw1/reducer.py -file /mnt/c/tmp/semenova_hw1/combiner.py -mapper "python3 mapper.py" -reducer "python3 reducer.py" -combiner "python3 combiner.py"
sudo /usr/local/hadoop/bin/hdfs dfs -cat /output/out/*