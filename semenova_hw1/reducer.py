
# coding: utf-8

# In[ ]:


#!/usr/bin/env python
"""reducer.py"""
import sys

def do_reduce(word, counts, values): 
    return word, sum(values)/sum(counts),sum(values)
 
prev_key = None 
values = [] 
counts = []
 
for line in sys.stdin: 
    key, count, value = line.split("\t") 
    if key != prev_key and prev_key is not None: 
        result_key, result_count, result_value = do_reduce(prev_key, counts, values) 
        print(result_key + "\t" + str(result_count) + "\t" + str(result_value)) 
        values = [] 
        counts = []
    prev_key = key 
    values.append(int(value))
    counts.append(int(count))

if prev_key is not None: 
    result_key, result_count, result_value = do_reduce(prev_key, counts, values) 
    print(result_key + "\t" + str(result_count) + "\t" + str(result_value))  

