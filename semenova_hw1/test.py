#!/usr/bin/env python
"""test.py"""

import unittest
import mapper
import sys

# UNIT TEST
class MapperTest(unittest.TestCase):
    def test_mapper(self):
        log = '109.169.248.247 - - [12/Dec/2015:18:25:11 +0100] "GET /administrator/ HTTP/1.1" 200 4263 "-" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-"'
        expected = ('109.169.248.247',1,4263)
        result = []
        for output in mapper.gener(log):
            #print(output)
            result.append(output)
        self.assertEqual(expected, result[0])
		#print('01')
if __name__ == '__main__':
    unittest.main()