#!/usr/bin/env python
# coding: utf-8
from pyspark import SparkContext
from pyspark import SparkConf
from cassandra.cluster import Cluster
import datetime

ids = []

def scaler(ts,scale):
	ts = int(ts)
	if scale == '1s':
		dt = datetime.datetime.utcfromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
		ts = int(datetime.datetime.timestamp(datetime.datetime.strptime(dt,'%Y-%m-%d %H:%M:%S')))
	elif scale == '1m':
		dt = datetime.datetime.utcfromtimestamp(ts).strftime('%Y-%m-%d %H:%M:00')
		ts = int(datetime.datetime.timestamp(datetime.datetime.strptime(dt,'%Y-%m-%d %H:%M:%S')))
	elif scale == '1h':
		dt = datetime.datetime.utcfromtimestamp(ts).strftime('%Y-%m-%d %H:00:00')
		ts = int(datetime.datetime.timestamp(datetime.datetime.strptime(dt,'%Y-%m-%d %H:%M:%S')))
	elif scale == '1d':
		dt = datetime.datetime.utcfromtimestamp(ts).strftime('%Y-%m-%d 00:00:00')
		ts = int(datetime.datetime.timestamp(datetime.datetime.strptime(dt,'%Y-%m-%d %H:%M:%S')))
	return ts
	
def id_lister(x):
	print(x.metr_id)
	

cluster = Cluster() #Запускаем кластер
session = cluster.connect('julia_hw') #Подключаемся к keyspace
metric = session.execute('select * from metrics')#Импортируем данные
conf = SparkConf().setAppName("Stand Alone Python Script")
sc = SparkContext(conf = conf)#создаём spark context

metrics = sc.parallelize(metric) #Загружаем в RDD
#Аггрегируем
#Стадия map
_id = metrics.map(lambda x: (x.metr_id,1))
_vals = metrics.map(lambda x: (x.metr_id,x.value))
id_tst = metrics.map(lambda x: (x.metr_id,x.timestamp))

#Стадия reduce		
count_id = _id.reduceByKey(lambda x,z: x + z)
sum_vals = _vals.reduceByKey(lambda x,z: x + z)
ids_tst = id_tst.reduceByKey(lambda x,z: x)

count_id = count_id.collect()
sum_vals = sum_vals.collect()
ids_tst = id_tst.collect()
out = []

for i in range(len(sum_vals)):
	sub_out = {}
	mean = int(sum_vals[i][1]/count_id[i][1])
	sub_out['metrics_id'] = count_id[i][0]
	sub_out['timestamp'] = ids_tst[i][1]
	sub_out['scale'] = '1m'
	sub_out['value'] = mean
	out.append(sub_out)
	

#Запись в cassandra
try:
	session.execute('CREATE ColumnFamily aggregated_metrics (metrics_id int PRIMARY KEY, timestamp bigint, scale VARCHAR, value int)')
except:
	session.execute('DROP TABLE aggregated_metrics')
	session.execute('CREATE ColumnFamily aggregated_metrics (metrics_id int PRIMARY KEY, timestamp bigint, scale VARCHAR, value int)')

for row in out:
	session.execute("INSERT INTO aggregated_metrics(metrics_id, timestamp, scale, value) VALUES ({0},{1},'1m',{2})".format(row['metrics_id'],row['timestamp'],row['value']))

print('---------------------------------------------------SUCCESS----------------------------------------------------')