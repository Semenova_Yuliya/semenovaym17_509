#!/usr/bin/env python
# coding: utf-8

import generator
import bash_scripting

if __name__ == '__main__':

	help = '''	Введите нужную комвнду

	g        - Сгенерировать файл с данными
	sh-file  - Показать файл с данными
	cass-ks  - Развернуть Cassandra KEYSPACE
	agg      - Ввести метрику для аггрегирования
	sh-out   - Показать результат аггрегирования
	cass-c   - Удалить Cassandra KEYSPACE
	exit     - Завершить
	'''
	# Создаём директорию, куда положим файл с данными 
	try:
		bash_scripting.rm_datadir()
		bash_scripting.data_dir()
	except:
		bash_scripting.rm_datadir()
		bash_scripting.data_dir()
		
	command = ''
	while command != 'exit':
	
		#начало программы
		try:
			print(help)
			command = input('Комманда: ')
		
			if command == 'g':
				generator.generate()
				
			if command == 'sh-file':
				bash_scripting.show_file()

			if command == 'cass-ks':
				bash_scripting.start_cassandra()

			if command == 'agg':
				bash_scripting.aggregate()

			if command == 'sh-out':
				bash_scripting.show_out()
			
			if command == 'cass-c':
				bash_scripting.drop_keyspace()
				
		except Exception as e:
			print(str(e))
	
	#удаление KEYSPACE после завершения работы
	try:
		bash_scripting.drop_keyspace()
	except:
		print('end')