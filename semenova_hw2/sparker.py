#!/usr/bin/env python
# coding: utf-8
from pyspark import SparkContext
from pyspark import SparkConf
from cassandra.cluster import Cluster
import time
from random import randrange
import datetime
#from cassandra.cluster import Cluster

def scaler(ts,scale):
	ts = int(ts)
	if scale == '1s':
		dt = datetime.datetime.utcfromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
		ts = int(datetime.datetime.timestamp(datetime.datetime.strptime(dt,'%Y-%m-%d %H:%M:%S')))
	elif scale == '1m':
		dt = datetime.datetime.utcfromtimestamp(ts).strftime('%Y-%m-%d %H:%M:00')
		ts = str(int(datetime.datetime.timestamp(datetime.datetime.strptime(dt,'%Y-%m-%d %H:%M:%S'))))
	elif scale == '1h':
		dt = datetime.datetime.utcfromtimestamp(ts).strftime('%Y-%m-%d %H:00:00')
		ts = int(datetime.datetime.timestamp(datetime.datetime.strptime(dt,'%Y-%m-%d %H:%M:%S')))
	elif scale == '1d':
		dt = datetime.datetime.utcfromtimestamp(ts).strftime('%Y-%m-%d 00:00:00')
		ts = int(datetime.datetime.timestamp(datetime.datetime.strptime(dt,'%Y-%m-%d %H:%M:%S')))
	return ts
	
#file.write(str(i) +',' +str(randrange(1,ids + 1)) + ',' + str(int(time.time())) + ',' + str(randrange(1,501,1)) + '\n')

cluster = Cluster() #Запускаем кластер
session = cluster.connect('julia_hw') #Подключаемся к keyspace
metric = session.execute('select * from metrics')#Импортируем данные
scale_name = session.execute('select * from metrics_scale')
conf = SparkConf().setAppName("Stand Alone Python Script")
sc = SparkContext(conf = conf)#создаём spark context

scales = []
for row in scale_name:
	scales.append(str(row.scale))
input_scale = scales[0]

metrics = sc.parallelize(metric) #Загружаем в RDD

scaled_metrics = metrics.map(lambda x: (x.metr_id,scaler(x.timestamp,input_scale),x.value))

counts = scaled_metrics.map(lambda x:((x[0],x[1]),1))
vals = scaled_metrics.map(lambda x:((x[0],x[1]),x[2]))

quantites = counts.reduceByKey(lambda x,y: x + y)
sums = vals.reduceByKey(lambda x,y: x + y)

quantites = quantites.collect()
sums = sums.collect()

out = []

for i in range(len(quantites)):
	sub_out = {}
	mean = int(sums[i][1]/quantites[i][1])
	sub_out['metrics_id'] = quantites[i][0][0]
	sub_out['timestamp'] = quantites[i][0][1]
	sub_out['scale'] = input_scale
	sub_out['value'] = mean
	out.append(sub_out)
for r in out:
	print(r)
try:
	session.execute('CREATE ColumnFamily aggregated_metrics (RowN int,metrics_id int PRIMARY KEY, timestamp bigint, scale VARCHAR, value int)')
except:
	session.execute('DROP TABLE aggregated_metrics')
	session.execute('CREATE ColumnFamily aggregated_metrics (RowN int PRIMARY KEY,metrics_id int, timestamp bigint, scale VARCHAR, value int)')
i = 0
for row in out:
	session.execute("INSERT INTO aggregated_metrics(RowN,metrics_id, timestamp, scale, value) VALUES ({0},{1},{2},'{3}',{4})"
					.format(i,row['metrics_id'],row['timestamp'],row['scale'],row['value']))
	i += 1
	
try:
	session.execute('CREATE ColumnFamily aggregated_metrics (RowN int,metrics_id int PRIMARY KEY, timestamp bigint, scale VARCHAR, value int)')
except:
	session.execute('DROP TABLE aggregated_metrics')
	session.execute('CREATE ColumnFamily aggregated_metrics (RowN int PRIMARY KEY,metrics_id int, timestamp bigint, scale VARCHAR, value int)')

i = 0
for row in out:
	session.execute("INSERT INTO aggregated_metrics(RowN,metrics_id, timestamp, scale, value) VALUES ({0},{1},{2},'{3}',{4})"
					.format(i,row['metrics_id'],row['timestamp'],row['scale'],row['value']))
	i += 1

print('---------------------------------------------------SUCCESS----------------------------------------------------')