#!/usr/bin/env python
# coding: utf-8
import time
from random import randrange
import os

def generate():
	#Определяем директорию, куда сохранится файл
	script_dir = os.path.dirname(__file__) 
	rel_path = 'data/metrics.txt'
	abs_file_path = os.path.join(script_dir, rel_path)
	
	#Принимаем значения числя строк и id
	count = int(input('Введите количество строк: '))
	ids = int(input('Введите максимальный id:    '))
	n = int(input('Введоте разброс timestamp: '))
	
	start_time = time.time()
	
	#Генерируем файл
	#открываем новый файл в дирректории
	file =  open(abs_file_path,'w')
	#Вписываем название полей
	file.write('id'+','+'metr_id'+','+'timestamp'+','+'value'+'\n')
	
	#создаёи случайные записи со случайными значениями id от 1 до ids + 1  и value от 1 до 501
	for i in range(count):
			file.write(str(i) +',' +str(randrange(1,ids + 1)) + ',' + str(int(time.time()) + randrange(-n,n,1)) + ',' + str(randrange(1,501,1)) + '\n')
			#time.sleep(0.31)
	print("Скрипт выполнен за {:.3f} с".format(time.time() - start_time))