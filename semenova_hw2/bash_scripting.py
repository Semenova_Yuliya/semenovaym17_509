#!/usr/bin/env python

import os
import subprocess

script_dir = os.path.dirname(__file__) 


def show_file():
	subprocess.call(script_dir + '/bash_scripts/show_file.sh',shell=True)
	
def start_cassandra():
	subprocess.call(script_dir + '/bash_scripts/start_cassandra.sh',shell=True)
	

def data_dir():
	subprocess.call(script_dir + '/bash_scripts/data_dir.sh',shell=True)

def rm_datadir():
	subprocess.call(script_dir + '/bash_scripts/rm_datadir.sh',shell=True)

def aggregate():
	scale = str(input('Input scale (1s,1m,1h,1d):  '))
	scaling = open(script_dir + '/cql/scaling.txt','w')
	scaling.write("use julia_hw;\nINSERT INTO metrics_scale (id,scale) values (1,'{0}');\nselect * from metrics_scale;".format(scale))
	command = "INSERT INTO metrics_scale (id,scale) values (1,'{0}')".format(scale)
	subprocess.call('/usr/local/cassandra/apache-cassandra-2.1.20/bin/cqlsh -k julia_hw -e "{0}"'.format(command),shell=True)
	#subprocess.call(['sudo /usr/local/spark-2.4.0-bin-hadoop2.7/bin/spark-submit ~/cass.py'],shell=True)
	#subprocess.call('bin/spark-submit ~/cass.py',shell=True)

def show_out():
	subprocess.call(script_dir + '/bash_scripts/show_out.sh',shell=True)

def drop_keyspace():
	subprocess.call(script_dir + '/bash_scripts/drop_keyspace.sh',shell=True)